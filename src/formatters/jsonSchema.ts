import { Options } from '../typings/options';
import { Schema } from '../typings/schema';

const chooseLanguage = (
  strings: { [lang: string]: string },
  languages?: Array<string>
) => {
  if (languages && languages.length) {
    for (let i = 0; i < languages.length; i++) {
      if (typeof strings[languages[i]] !== 'undefined') {
        return strings[languages[i]];
      }
    }
  }

  const keys = Object.keys(strings);
  if (keys.length) {
    return strings[keys[0]];
  }

  return null;
};

const schemaKeys = ['additionalProperties', 'items', 'additionalItems'];

const formatPart = (part: Schema, options: Options) => {
  const formatted = { ...part };

  if (typeof formatted['$ref'] === 'string' && options.resolve) {
    formatted['$ref'] = options.resolve(formatted['$ref']);
  }

  if (typeof formatted.title === 'object') {
    formatted.title = chooseLanguage(formatted.title, options.languages);
  }

  if (typeof formatted.description === 'object') {
    formatted.description = chooseLanguage(
      formatted.description,
      options.languages
    );
  }

  if (typeof formatted.properties === 'object') {
    formatted.properties = { ...formatted.properties };

    const keys = Object.keys(formatted.properties);

    for (let i = 0; i < keys.length; i++) {
      formatted.properties[keys[i]] = formatPart(
        formatted.properties[keys[i]],
        options
      );
    }
  }

  for (let i = 0; i < schemaKeys.length; i++) {
    const value = formatted[schemaKeys[i]];
    if (Array.isArray(value)) {
      for (let j = 0; j < value.length; j++) {
        if (typeof value[j] === 'object') {
          value[j] = formatPart(value[j], options);
        }
      }
    } else if (typeof value === 'object') {
      formatted[schemaKeys[i]] = formatPart(value, options);
    }
  }

  return formatted;
};

export const format = (schema: Schema, options: Options = {}): Schema => {
  return formatPart(schema, options);
};

export const parse = (schema: Schema): Schema => schema;
