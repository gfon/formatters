# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [unreleased]

Initial version

[unreleased]: https://gitlab.com/MeldCE/first-draft/compare/master...initial
[0.1.0]: https://gitlab.com/bytesnz/repo-utils/tree/v0.1.0
