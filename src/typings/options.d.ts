export interface Options {
  /// Ordered preference to schema language
  languages?: Array<string>;
  /// Base URI for middleware
  baseUri?: string;
  /// Function to use to resolve references in the schema
  resolve?: (uri: string) => string;
  /// Pretty print schema
  pretty?: boolean;
}
