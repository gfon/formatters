interface ParsedPart {
  value: string;
  weight: number;
  parameters: {
    [key: string]: string;
  };
}

/**
 * Parse an HTTP header value
 *
 * @param headerValue {string} Header value to parse
 */
export const parseHeader = (headerValue: string): Array<ParsedPart> => {
  if (!headerValue) {
    return null;
  }

  const headerParts = headerValue.split(/ *, */);
  const headerOptions = [];

  for (let i = 0; i < headerParts.length; i++) {
    const parts = headerParts[i].split(/ *; */);

    const option = {
      value: parts.shift(),
      weight: 1,
      parameters: {}
    };

    for (let j = 0; j < parts.length; j++) {
      const parameters = parts[j].split(/ *= */);

      if (parameters[0] === 'q') {
        const weight = Number(parameters[1]);

        if (!isNaN(weight)) {
          option.weight = weight;
        }
      } else {
        option.parameters[parameters[0]] = parameters[1];
      }
    }

    headerOptions.push(option);
  }

  headerOptions.sort((a, b) => {
    if (a.weight < b.weight) {
      return 1;
    }

    if (a.weight > b.weight) {
      return -1;
    }

    return 0;
  });

  return headerOptions;
};

export const urlResolve = (start: string, ...parts: Array<string>): string => {
  if (start.startsWith('file:')) {
    start = start.replace(/^file:\/\/\/*(.*)$/, (match, rest) => {
      if (rest) {
        parts.unshift(rest);
      }

      return 'file:///';
    });
  } else {
    start = start.replace(
      /^([a-z]+):\/\/\/*(?:([^/]+)(?:\/(.*))?)?$/,
      (match, protocol, domain, rest) => {
        if (rest) {
          parts.unshift(rest);
        }

        return protocol + '://' + (domain ? domain + '/' : '');
      }
    );
    start = start.replace(/^\/\/\/*(.*)$/, (match, rest) => {
      if (rest) {
        parts.unshift(rest);
      }

      return '//';
    });
  }
  if (parts.length && !start.endsWith('/')) {
    start += '/';
  }

  let cleanParts = [];
  for (let i = 0; i < parts.length; i++) {
    cleanParts = cleanParts.concat(
      typeof parts[i] === 'string' ? parts[i].split(/\/+/) : parts[i]
    );
  }

  parts = [];
  for (let i = 0; i < cleanParts.length; i++) {
    if (
      typeof cleanParts[i] === 'undefined' ||
      ['', '.', null].indexOf(cleanParts[i]) !== -1
    ) {
      continue;
    }

    if (cleanParts[i] === '..') {
      parts.pop();
      continue;
    }

    parts.push(cleanParts[i]);
  }

  return start + parts.join('/');
};
