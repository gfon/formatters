const eslintrc = require('./.eslintrc.commit.cjs');

module.exports = {
  ...eslintrc,
  rules: {
    ...eslintrc.rules,
    'no-console': 'warn',
    'no-debugger': 'warn'
  }
};
