# schema-formatters <!=package.json version>

Schema formatters for converting data into different schema formats.

[![first-draft on NPM](https://bytes.nz/b/first-draft/npm)](https://npmjs.com/package/first-draft)
[![developtment time](https://bytes.nz/b/first-draft/custom?color=yellow&name=development+time&value=~250+hours)](https://gitlab.com/MeldCE/first-draft/blob/master/.tickings)
[![contributor covenant](https://bytes.nz/b/first-draft/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/MeldCE/first-draft/blob/master/code-of-conduct.md)

## Schema Formatters

The following formatters are available to format schema into/from a particular
format from/to the [extended JSON Schema format](#input-format).

- [JSON Schema][json-schema] to
- [JSON-LD][json-ld]
- [RDFa][rdfa]
- [Turtle][turtle]
- [Typescript interfaces][typescript]

### Functions

#### Format

```typescript
<
format(schema: Schema, lang: string | Array<string>, schemas?: { [id: string]: Schema) => string
format(schema: string, lang: string | Array<string>, schemas: { [id: string]: Schema) => string
```

### Input Format

The input format is largely [JSON Schema][json-schema], but allowing titles
and descriptions to be specified in
[language maps](https://www.w3.org/TR/json-ld/#dfn-language-map) as in
[JSON-LD][json-ld]

## Server Middleware

<!=CHANGELOG.md>

[json-schema]: https://json-schema.org/
[json-ld]: https://json-ld.org
[http-accept]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept
[turtle]: https://en.wikipedia.org/wiki/Turtle_%28syntax%29
