type LangString =
  | string
  | {
      [lang: string]: string;
    };

interface BaseSchema {
  title?: LangString;
  description?: LangString;
  $ref?: string;
}

interface ObjectSchema extends BaseSchema {
  type: 'object';
  properties?: { [field: string]: Schema };
  required?: Array<string>;
  additionalProperties?: boolean | { [field: string]: Schema };
  minProperties?: number;
  maxProperties?: number;
}

interface StringSchema extends BaseSchema {
  type: 'string';
  pattern?: string;
  minLength?: number;
  maxLength?: number;
}

interface NumberSchema extends BaseSchema {
  type: 'number' | 'integer';
  minimum?: number;
  maximum?: number;
}

interface BooleanSchema extends BaseSchema {
  type: 'boolean';
}

interface ArraySchema extends BaseSchema {
  type: 'array';
  items?: Schema | Array<Schema>;
  minItems?: number;
  maxItems?: number;
}

export type Schema =
  | ObjectSchema
  | StringSchema
  | BooleanSchema
  | NumberSchema
  | ArraySchema;
