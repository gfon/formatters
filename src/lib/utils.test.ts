import * as test from 'japa';

import * as utils from './utils';

test.group('parseHeader()', () => {
  test('parses and orders accept headers', (assert) => {
    assert.deepEqual(
      [
        {
          value: 'test/1',
          weight: 1,
          parameters: {
            six: 'seven',
            eight: 'nine'
          }
        },
        {
          value: 'test/3',
          weight: 1,
          parameters: {
            one: 'two'
          }
        },
        {
          value: 'test/2',
          weight: 0.5,
          parameters: {}
        }
      ],
      utils.parseHeader(
        'test/1;six=seven;eight = nine, test/2; q=0.5,test/3; one=two'
      )
    );
  });
});

test.group('urlResolve()', () => {
  test('fixes file urls', (assert) => {
    assert.equal(
      utils.urlResolve('file://test/here'),
      'file:///test/here',
      'does not add a extra slash to a single string file url'
    );

    assert.equal(
      utils.urlResolve('file://', 'test', 'here'),
      'file:///test/here',
      'does not add a extra slash to a mulitple string file url'
    );
  });

  test('fixes http(s) urls', (assert) => {
    assert.equal(
      utils.urlResolve('http:///test/here'),
      'http://test/here',
      'does not remove an extra slash to a single string file url'
    );

    assert.equal(
      utils.urlResolve('https:///test/here'),
      'https://test/here',
      'does not remove an extra slash to a single string file url'
    );

    assert.equal(
      utils.urlResolve('http:///', 'test', 'here'),
      'http://test/here',
      'does not remove an extra slash to a mulitple string file url'
    );

    assert.equal(
      utils.urlResolve('https:///', 'test', 'here'),
      'https://test/here',
      'does not remove an extra slash to a mulitple string file url'
    );
  });

  test('resolves .. parts', (assert) => {
    assert.equal(
      utils.urlResolve('http://example.com/some/path', '../../another/one'),
      'http://example.com/another/one',
      'does not resolve .. correctly'
    );

    assert.equal(
      utils.urlResolve(
        'http://example.com/some/path',
        '..',
        '..',
        'another/one'
      ),
      'http://example.com/another/one',
      'does not resolve .. correctly'
    );
  });

  test('ignores . and // in parts', (assert) => {
    assert.equal(
      utils.urlResolve('http://example.com/some/path', './/another/one'),
      'http://example.com/some/path/another/one',
      'does not ignore . and // correctly'
    );

    assert.equal(
      utils.urlResolve(
        'http://example.com/some/path',
        '.',
        '//',
        'another/one'
      ),
      'http://example.com/some/path/another/one',
      'does not ignore . and // correctly'
    );
  });
});
