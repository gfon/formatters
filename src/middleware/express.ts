import { Options } from '../typings/options';
import { Schema } from '../typings/schema';

import { parseHeader, urlResolve } from '../lib/utils';
import { format as jsonSchemaFormat } from '../formatters/jsonSchema';
/**
 * Express middleware to serve schema in different formats
 *
 * @param getSchema Async function to get a requested schema. Should return
 *   undefined if the requested schema does not exist
 * @param options Middleware options
 */
export default (
  getSchema: (uri: string) => Promise<Schema>,
  options: Options = {}
) => async (req, res, next) => {
  const host = req.get('x-forwarded-host') || req.hostname;
  const protocol = req.get('x-forwarded-proto') || req.protocol;

  const accepts = parseHeader(req.get('accept'));
  const languages = parseHeader(req.get('accept-language'))?.map(
    (l) => l.value
  );

  if (!accepts) {
    next();
    return;
  }

  const schema = await getSchema(req.path);

  if (!schema) {
    res.status = 404;
    res.end();
    return;
  }

  for (let i = 0; i < accepts.length; i++) {
    switch (accepts[i].value) {
      case 'application/json': {
        // JSON Schema
        res.status = 200;
        const formatted = jsonSchemaFormat(schema, {
          ...options,
          languages: languages || options.languages,
          resolve: (uri: string) => {
            if (uri.startsWith('//')) {
              return protocol + uri;
            } else if (uri.startsWith('/')) {
              return urlResolve(
                protocol + '://' + host,
                options.baseUri || null,
                uri
              );
            } else if (uri.startsWith('.')) {
              return urlResolve(
                protocol + '://' + host,
                options.baseUri || null,
                req.path,
                uri
              );
            }
          }
        });

        if (options.pretty) {
          res.end(JSON.stringify(formatted, null, 2));
        } else {
          res.end(JSON.stringify(formatted));
        }

        return;
      }
    }
  }

  next();
};
