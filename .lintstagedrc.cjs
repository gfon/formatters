module.exports = {
  '{src/README.md,CHANGELOG.md,package.json}': [
    () => 'utils/mdFileInclude.cjs src/README.md README.md',
    'git add README.md'
  ],
  '*.{scss,md}': ['prettier --write'],
  '*.{ts,json,js,cjs}': [
    'prettier --write',
    'eslint -c .eslintrc.commit.cjs --fix'
  ]
};
